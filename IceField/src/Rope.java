public class Rope implements Item {
    public void saveFallenFigure(Figure saverFigure, Figure fallenFigure) {
        fallenFigure.SetIsFallen(false);
        fallenFigure.SetIceberg(SaverFigure.GetIceberg());
        saverFigure.removeItem(this);
    }
}

public class Shovel implements Item {
    public void removeSnow(Figure figure, Iceberg iceberg, Shovel shovel) {

        iceberg.removeSnow(2);
        if (iceberg.GetSnowAmount() <= 0) {
            iceberg.RevealItem();
        }
        figure.removeItem(shovel);
    }
}

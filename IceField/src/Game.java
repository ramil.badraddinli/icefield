import java.util.ArrayList;
import java.util.Scanner;

public class Game {
    private int number_of_players;
    private int number_of_figures;
    private ArrayList<Player>players;
    private ArrayList<Figure>figures;

    public Game() {
        this.number_of_players = 0;
        this.players = new ArrayList<Player>();
    }

    public void initializeParameters() {
        System.out.println("Enter the number of players: ");
        Scanner in = new Scanner(System.in);
        int number_of_players = in.nextInt();
        in.nextLine();
        for (int i = 1; i <= number_of_players; ++i) {
            System.out.println("Name of player" + i + ": ");
            String username = in.nextLine();
            Player player = new Player(username);
            players.add(player);
         }

         System.out.println("Enter the number of figures: ");
         int number_of_figures = in.nextInt();
         in.nextLine();
         for (int i=1; i <= number_of_figures; ++i) {
             if (i%2 == 1) {
                 Eskimos eskimos = new Eskimos();
                 figures.add(eskimos);
             } else {
                 Explorer explorer = new Explorer();
                 figures.add(explorer);
             }
         }
    }

    public void startGame() {

    }

    public void winGame() {

    }

    public void loseGame() {

    }

    public void activateBlizzard() {

    }

    public void checkSituation() {

    }
}
